from app import db, app
from sqlalchemy import func
from sqlalchemy.orm import contains_eager
import os.path
import datetime
import flask_sqlalchemy
from models import *

def afficher_queries():
    for statement, parameters, start, duration, context in flask_sqlalchemy.get_debug_queries():
        print(statement.replace("\n"," ") , parameters)

@app.cli.command()
def initdb():
    # Creation des bases
    db.create_all()
    afficher_queries()
